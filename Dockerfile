FROM node:8.10.0-alpine
ENV http_proxy http://proxy.true.th:80
ENV https_proxy http://proxy.true.th:80
WORKDIR /app
COPY package.json ./
RUN npm install --prod
COPY . .
EXPOSE 3000
ENV http_proxy ""
ENV https_proxy ""
CMD ["npm", "run", "prod"]