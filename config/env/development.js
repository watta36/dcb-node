module.exports = {
  charge_server: {
    port: "7565",
    host: "172.16.3.211"
  },
  credit_server: {
    port: "7566",
    host: "172.16.3.211"
  },
  ccp_server: {
    port: "7567",
    host: "172.16.3.211"
  },
  cdb_server: {
    port: "10080",
    host: "172.19.189.55"
  }
};
