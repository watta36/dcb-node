const services = require("./../services/index");
const env = require("./../../config/env/development");

const bad_request_message_res='Invalid or Bad Request';

async function dcbRoute(request,response) {

  let {order:{orderType}} = request;
  // const orderType = request.order.orderType;

  //////////////////// arARBIRChargeServices //////////////////////
  // console.log('order type :',orderType);
  if(orderType === '11011'){
    let res ;
    try{
    res =  services.grpc.AR_ARBIRChargeServices.arARBIRChargeServices(
      request
    ).then(function (resp){
      console.log("Request time 1");
      const handleres = handleResponsePostPaid(resp);
      console.log("handleres" ,handleres);
      response.send(handleres);
    });
    console.log("Request time 2");
  }catch (err) {
    console.log('err',err)
    response.status(200);
    return errorRes('','FAILED',400,bad_request_message_res);
  }
  // return handleResponsePostPaid(res);
  }

//////////////////// createInvoiceLevelCreditService //////////////////////
if(orderType === '11012'){
  let res ;
  try{
  res = services.grpc.AR_ARBIRCreditServices.createInvoiceLevelCreditService(
    request
  ).then(function (resp){
    console.log("Request time 1");
    const handleres = handleResponsePostPaid(resp);
    console.log("handleres" ,handleres);
    response.send(handleres);
  });
  console.log("Request time 2");
}catch (err) {
  console.log('err',err)
  response.status(200);
  return errorRes('','FAILED',400,bad_request_message_res);
}
// return handleResponse(res);
}

//////////////////// ccpService //////////////////////
if(orderType === '12004'){
  let res ;
  let {
  customer:{
    account:[{
      product:[{productNumber}]
    }]
  }} = request;
  console.log('productNumber : '+productNumber);
  const cdbData = await services.getChain.getChainFromCdb(env.cdb_server.host,env.cdb_server.port,productNumber);
  console.log('cdbData : '+cdbData);
  const arr = cdbData.split("|");
  console.log('cdbData : ',cdbData);
  if(arr[6] === '' || arr[6] === undefined){
    response.status(200);
    return errorRes('','FAILED',400,bad_request_message_res)
  }
  try{
    
    res =  services.grpc.ccp_Charge_RefundServices.ccpDeductionService(
        request,arr[6]+'',response
      ).then(function (resp){
        console.log("Request time 1");
        const handleres = handleResponse(resp);
        console.log("handleres" ,handleres);
        response.send(handleres);
      });
  }catch (err) {
    console.log('err',err)
    response.status(200);
    return errorRes('','FAILED',400,bad_request_message_res);
  }
  // return handleResponse(res);
}

//////////////////// ccpService //////////////////////
if(orderType === '12005'){
  let res ;
  let {
    customer:{
      account:[{
        product:[{productNumber}]
      }]
    }} = request;
  console.log('productNumber : '+productNumber);
  const cdbData = await services.getChain.getChainFromCdb(env.cdb_server.host,env.cdb_server.port,productNumber);
  const arr = cdbData.split("|");
  console.log('cdbData : ',cdbData);
  if(arr[6] === '' || arr[6] === undefined){
    response.status(200);
    return errorRes('','FAILED',400,bad_request_message_res)
  }
  try{
    res =  services.grpc.ccp_Charge_RefundServices.ccpRechargingService(
    request,arr[6]+'',response
  ).then(function (resp){
    console.log("Request time 1");
    const handleres = handleResponse(resp);
    console.log("handleres" ,handleres);
    response.send(handleres);
  });
  }catch (err) {
    console.log('err',err)
    response.status(200);
    return errorRes('','FAILED',400,bad_request_message_res);
  }
  // return handleResponse(res);
}
}

function errorRes(omxTrackingId,result,errorCode,errorMsg){
  return {
    omxTrackingId:omxTrackingId ,
    result: result,
    errCode: errorCode,
    errMsg: errorMsg
}
}

function handleResponsePostPaid(response){
  console.log('response : ',response)
  if(response.result == 'SUCCESS'){
    return postPaidSuccessRes(response.omxTrackingId,response.result,response.resultResp.invoiceNo)
  } else {
    return errorRes(response.omxTrackingId,response.result,response.errCode,response.errMsg)
  }
}

function handleResponse(response){
  console.log('response : ',response)
  if(response.result == 'SUCCESS'){
    return successRes(response.omxTrackingId,response.result)
  } else {
    return errorRes(response.omxTrackingId,response.result,response.errCode,response.errMsg)
  }
}

function postPaidSuccessRes(omxTrackingId,result,ban){
  return {
    omxTrackingId:omxTrackingId ,
    result: result,
    resultResp: {
      invoiceNo: ban
  }
}
}

function successRes(omxTrackingId,result){
  return {
    omxTrackingId:omxTrackingId ,
    result: result
}
}

module.exports = {
  dcbRoute
};
