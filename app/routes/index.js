// const Logger = require("./../common/logger");
const dcb = require("./dcb.route");
// var logger = require('fluent-logger').createFluentSender('omx-dcb*', {
//   host: '172.16.3.212',
//   port: 24233,
//   timeout: 3.0,
//   reconnectInterval: 600000 // 10 minutes
// });

// logger.on('error', (error) => {
//   console.log(error);
// });
// logger.on('connect', () => {
//   console.log('connected!');
// });

module.exports = function(app) {

  app.post('/dcbService', function({body:{submitOrderRequest}}, res) {
    var start = new Date();
      dcb.dcbRoute(submitOrderRequest,res).then(function (response){
        console.log("Request time", new Date() - start, "ms");
        // res.send(response);
      });

      
  });

  app.post('/omxService', function({body:{submitOrderRequest}}, res,next) {
    dcb.dcbRoute(submitOrderRequest,res).then(function (response){
      // console.log('response : ',response);
      // res.send(response);
    });
    console.log('main');

  });

  

  app.post('/testLog', async function(req, res) {
    // logger.createFluentSender('omx-dcb', {
    //   host: '172.16.3.212',
    //   port: 24233,
    //   timeout: 3.0,
    //   reconnectInterval: 600000 // 10 minutes
    // });
    // console.log('logger : ',logger);
    // await logger.emit({message:"1111111111111111111111"});




    var winston = require('winston');
    var config = {
      host: '172.16.3.212',
      port: 24233,
      timeout: 3.0,
      requireAckResponse: true // Add this option to wait response from Fluentd certainly
    };
    var fluentTransport = require('fluent-logger').support.winstonTransport();
    var logger1 = winston.createLogger({
        transports: [new fluentTransport('mytag', config), new (winston.transports.Console)()]
    });

    logger1.on('logging', (transport, level, message, meta) => {
      if (meta.end && transport.sender && transport.sender.end) {
        transport.sender.end();
      }
    });

    logger1.log('info', 'this log record is sent to fluent daemon');
    logger1.info('this log record is sent to fluent daemon');
    logger1.info('end of log message', { end: true });
    res.send("");
  });
  
};
