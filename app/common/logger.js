//const config = require('./../../config/config')().config;

//error,
//warn,
//info,

async function log(type,action,message,customer_id,user,request_id) {

    var dt = new Date();

    this.log_print.timestamp  = dt.toISOString();
    this.log_print.level   = type;
    this.log_print.action = action||this.log_print.action;
    this.log_print.customer_id    = customer_id||this.log_print.customer_id||'0000000000000';
    this.log_print.user   = user||this.log_print.user;
    this.log_print.request_id = request_id||this.log_print.request_id;
    this.log_print.message = message||this.log_print.message;
    //this.path:this.path,
    //this.method:this.method,
    //this.size:(this.size*0.75-1),
    //this.count:this.count,
    console.log(JSON.stringify(this.log_print));
}

module.exports = function(req) {

    var logger = {
        log,
        type:{error:'error',warn:'warn',info:'info',success:'success'},
        log_print:{system:'dcb-api'},
    }

    if(req){
        logger.log_print.path = req.path;
        logger.log_print.method = req.method;
    }

    return logger;

};