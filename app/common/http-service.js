const request = require('request');

function callHttp(url){
    var options = {
        url: url,
        headers: {
            'User-Agent': 'request'
        }
    };

    return new Promise(function(resolve, reject) {
        // Do async job
           request.get(options, function(err, resp, body) {
               if (err) {
                   reject(err);
               } else {
                   resolve(body);
               }
           });
       });
}

module.exports = {
    callHttp
  };