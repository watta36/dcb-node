const grpc = require('./grpc/index');
const getChainFromCdb = require('./cdbService');

module.exports = {
    grpc,
    getChain:getChainFromCdb
};