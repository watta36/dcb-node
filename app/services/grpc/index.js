const {
  l9createImmediateChargePaymentRequestServices
} = require("./arARBIRChargeServices");
const {
  createInvoiceLevelCreditService
} = require("./arARBIRCreditServices");
const {
  ccpRechargingService,ccpDeductionService
} = require("./ccpServices");
module.exports = {
  AR_ARBIRChargeServices: {
    arARBIRChargeServices: l9createImmediateChargePaymentRequestServices
  },
  AR_ARBIRCreditServices: {
    createInvoiceLevelCreditService:createInvoiceLevelCreditService
  },
  ccp_Charge_RefundServices: {
    ccpRechargingService:ccpRechargingService,
    ccpDeductionService:ccpDeductionService
  }
};
