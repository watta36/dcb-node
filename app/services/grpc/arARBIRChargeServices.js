var PROTO_PATH = __dirname + "../../../../config/arARBIRChargeServices.proto";
const env = require("./../../../config/env/development");
const uuidv1 = require('uuid/v1');
var grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");
var packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
});
var hello_proto = grpc.loadPackageDefinition(packageDefinition)
  .io.grpc.arARBIRChargeServices;

 function l9createImmediateChargePaymentRequestServices(request) {
  return new Promise((resolve, reject) => {
  var client = new hello_proto.Greeter(
    env.charge_server.host + ":" + env.charge_server.port,
    grpc.credentials.createInsecure()
  );
  var start = new Date();
  var call = client.l9createImmediateChargePayment(function(error, stats) {
    if (error) {
      console.log("error :", error);
      console.log("Request time", new Date() - start, "ms");
      resolve(error);
    }
    console.log("Request time", new Date() - start, "ms");
    resolve(stats);
  });
    call.write(setRequest(request));
    call.end();
  });
 }

function setRequest(request){
  let {order:{channel='',orderId='',transactionId='',orderType='',orderDate=''},
  customer:{
    account:[{accountId='',
      charge:[{chargeCode='',amount='',descriptionTh='',descriptionEn=''}],
      product:[{productNumber=''}]
    }]
  }} = request;

  const l9createImmediateChargePaymentRequest = {
    accountInfoDt: {
      l9InvoiceDueDate: "",
      accountId: accountId,
      billingArrangementId: accountId
    },
    chargeRequestList: {
      chargeAmount: amount,
      l9TaxItemInfoArray: {
        taxAuthority: "COUNTRY",
        taxRate: "0",
        taxCode: "V00",
        taxAmount: "0.00",
        billingArrangementId: accountId,
        taxType: "auto"
      },
      chargesCustDt:{
        l9PrimResourceVal:productNumber,
        l9OrderId:orderId,
        l9DescriptionTh:descriptionTh,
        l9DescriptionEng:descriptionEn,
        l9ExtDateTime:orderDate
      },
      chargeCode: chargeCode,
      chgRevenueCode: "OC"
    },
    trackingId:uuidv1()
  };
  // console.log(l9createImmediateChargePaymentRequest);
  return l9createImmediateChargePaymentRequest;
}

module.exports = {
  l9createImmediateChargePaymentRequestServices
};
