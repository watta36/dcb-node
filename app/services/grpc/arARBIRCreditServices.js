var PROTO_PATH = __dirname + "../../../../config/arARBIRCreditServices.proto";
const env = require("./../../../config/env/development");
const uuidv1 = require('uuid/v1');
var grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");
var packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
});
var hello_proto = grpc.loadPackageDefinition(packageDefinition)
  .arARBIRCreditServices;

 function createInvoiceLevelCreditService(request) {
  return new Promise((resolve, reject) => {
  var client = new hello_proto.Greeter(
    env.credit_server.host + ":" + env.credit_server.port,
    grpc.credentials.createInsecure()
  );
  var start = new Date();
  var call = client.createInvoiceLevelCredit(function(error, stats) {
    if (error) {
      console.log("error :", error);
      console.log("Request time", new Date() - start, "ms");
      resolve(error);
    }
    console.log("Request time", new Date() - start, "ms");
    resolve(stats);
  });
    call.write(setRequest(request));
    call.end();
});
}

function setRequest(request){
  let {order:{
    orderDate,
    activityInfo:{
      activityReason
    }
  },
  customer:{
    account:[{
      accountId,
      refund:[{invoiceNo,amount}],
      product:[{productNumber}]
    }]
  }
} = request;
  const createInvoiceLevelCreditRequest = {
    invoiceIdInfoDt: {
      invoiceId:invoiceNo
    },
    accountIdInfoDt: {
      accountId:accountId
    },
    creditNoteIdInfoDt: {
      invoiceId:invoiceNo
    },
    invoiceLevelCreditDetailsScreenDt: {
      creditTypeOption:"auto",
      creditReason:activityReason,
      amount:amount,
      taxInclude:"0",
      customerCreditCustDt:{
        l9ExtDateTime:orderDate
      }
    },
    trackingId:uuidv1()
};
return createInvoiceLevelCreditRequest;
}

module.exports = {
    createInvoiceLevelCreditService
};
