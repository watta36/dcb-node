var PROTO_PATH = __dirname + "../../../../config/TrueWebServices.proto";
const env = require("./../../../config/env/development");
const uuidv1 = require('uuid/v1');
var grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");
var packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
});
var hello_proto = grpc.loadPackageDefinition(packageDefinition)
  .ccpTrueWebservice;

function ccpDeductionService(request,chainFromDCB,response) {
  return new Promise((resolve, reject) => {
    
    var client = new hello_proto.Greeter(
      env.ccp_server.host + ":" + env.ccp_server.port,
      grpc.credentials.createInsecure()
    );
    var start = new Date();
    var call = client.deduction(function(error, stats) {
      if (error) {
        console.log("error :", error);
        console.log("Request time", new Date() - start, "ms");
        // response.status(400);
        resolve(error);
      }
      // console.log("Request time", new Date() - start, "ms");
      console.log("responseFromDB : ", stats);
      resolve(stats);
    });
  
    call.write(setCcpDeductionService(request,chainFromDCB));
    call.end();
    });
}

function setCcpDeductionService(request,chainFromDCB){
  let c = chainFromDCB.toString();
  let {order:{authenUser='',authenPassword='',transactionId='',clientId='',user='',password='',smsSendInd=''},
  customer:{
    account:[{accountId='',
      charge:[{chargeCode='',amount=''}],
      product:[{productNumber=''}]
    }]
  }} = request;
  const deductionRequest = {
    deductionReqDto: {
      transactionSN:transactionId,
      cliendtId:clientId,
      user:user,
      password:password,
      serviceId:chargeCode,
      msisdn:'66' + productNumber[1] + productNumber.slice(2),
      amount:Math.imul(amount, 10000),
      msgType:"11",
      serviceType:"4",
      sendSmsFlag:smsSendInd
    },chain : c
    ,header:{
      authenUser:authenUser,
      authenPassword:authenPassword
    },
    trackingId:uuidv1()
};
return deductionRequest;
}

function ccpRechargingService(request,chainFromDCB,response) {
  return new Promise((resolve, reject) => {
    
    var client = new hello_proto.Greeter(
      env.ccp_server.host + ":" + env.ccp_server.port,
      grpc.credentials.createInsecure()
    );
    var start = new Date();
    var call = client.recharging(function(error, stats) {
      if (error) {
        console.log("error :", error);
        console.log("Request time", new Date() - start, "ms");
        // response.status(400);
        resolve(error);
      }
      // console.log("Request time", new Date() - start, "ms");
      // console.log("responseFromDB : ", stats);
      resolve(stats);
    });
  
    call.write(setCcpRechargingService(request,chainFromDCB));
    call.end();
    });
}

function setCcpRechargingService(request,chainFromDCB){
  let c = chainFromDCB.toString();
  let {order:{authenUser,authenPassword,transactionId,password,smsSendInd,activityInfo:{activityReason}},
  customer:{
    account:[{
      refund:[{amount}],
      product:[{productNumber}]
    }]
  }} = request;

  const rechargingRequest = {
    rechargingReqDto: {
      msisdn:'66' + productNumber[1] + productNumber.slice(2),
     acctResCode:'0',
     addBalance:Math.imul(amount, -10000),
     smsSendFlag:smsSendInd,
     transactionSN:transactionId,
     requestID:activityReason,
     userPwd:password
    },chain : c
    ,header:{
      authenUser:authenUser,
      authenPassword:authenPassword
    },
    trackingId:uuidv1()
};
return rechargingRequest;
}

module.exports = {
  ccpRechargingService,
  ccpDeductionService
};
