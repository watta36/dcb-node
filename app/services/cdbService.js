const services = require("./../common/http-service");


function getChainFromCdb(ip,port,phoneNumber){
    let phoneNumber66 = '66' + phoneNumber[1] + phoneNumber.slice(2);
    const url = 'http://'+ip+':'+port+'/cdbldap/query_cdb.php?action=getinfo&msisdn='+phoneNumber66;
    return services.callHttp(url);
}

module.exports = {
    getChainFromCdb
  };