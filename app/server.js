// server.js

// set up ======================================================================
// get all the tools we need
var express = require("express");
var session = require("express-session");
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 8080;

// routes ======================================================================
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.set('view engine', 'ejs'); 
require("./routes/index.js")(app);

// launch ======================================================================
app.listen(port);
console.log("The magic happens on port " + port);
